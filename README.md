# spring_batch_update_salary

Right now I am getting a issue. The issue is that the tables related to Spring Batch are not getting created during the
startup of the application. In the application.yaml file, I have configured the spring.batch.jdbc.initialize-schema to
always, which should normaly trigger the initialization of the Spring Batch schema during application startup. But it's
not working as expected. Will check later.