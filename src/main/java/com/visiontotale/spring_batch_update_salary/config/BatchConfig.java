package com.visiontotale.spring_batch_update_salary.config;

import com.visiontotale.spring_batch_update_salary.model.BankTransaction;
import com.visiontotale.spring_batch_update_salary.repository.BankTransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
public class BatchConfig {

  private final JobRepository jobRepository;
  private final PlatformTransactionManager platformTransactionManager;
  private final BankTransactionRepository bankTransactionRepository;

  @Bean
  public FlatFileItemReader<BankTransaction> itemReader() {
    FlatFileItemReader<BankTransaction> itemReader = new FlatFileItemReader<>();
    itemReader.setResource(new FileSystemResource("src/maim/resources/inputDataFile.csv"));
    itemReader.setName("csvReader");
    itemReader.setLinesToSkip(1);
    itemReader.setLineMapper(lineMapper());
    return itemReader;
  }

  @Bean
  public BankTransactionProcessor processor() {
    return new BankTransactionProcessor();
  }

  @Bean
  public RepositoryItemWriter<BankTransaction> writer() {
    RepositoryItemWriter<BankTransaction> writer = new RepositoryItemWriter<>();
    writer.setRepository(bankTransactionRepository);
    writer.setMethodName("save");
    return writer;
  }

  @Bean
  public Step importStep() {
    return new StepBuilder("csvImport", jobRepository)
        .<BankTransaction, BankTransaction>chunk(10, platformTransactionManager)
        .reader(itemReader())
        .processor(processor())
        .writer(writer())
        .build();
  }

  @Bean
  public Job runJob() {
    return new JobBuilder("importBankTransactions", jobRepository).start(importStep()).build();
  }

  private LineMapper<BankTransaction> lineMapper() {
    DefaultLineMapper<BankTransaction> lineMapper = new DefaultLineMapper<>();

    DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter(",");
    lineTokenizer.setStrict(false);
    lineTokenizer.setNames(
        "Transaction_ID", "Beneficiary_Name", "Transaction_Date", "Account_ID", "Amount");

    BeanWrapperFieldSetMapper<BankTransaction> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
    fieldSetMapper.setTargetType(BankTransaction.class);

    lineMapper.setLineTokenizer(lineTokenizer);
    lineMapper.setFieldSetMapper(fieldSetMapper);

    return lineMapper;
  }
}
