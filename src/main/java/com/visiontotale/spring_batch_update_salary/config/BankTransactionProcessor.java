package com.visiontotale.spring_batch_update_salary.config;

import com.visiontotale.spring_batch_update_salary.model.BankTransaction;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.batch.item.ItemProcessor;

public class BankTransactionProcessor implements ItemProcessor<BankTransaction, BankTransaction> {
  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Override
  public BankTransaction process(BankTransaction bankTransaction) throws Exception {
    bankTransaction.setTransactionDate(
        LocalDate.parse(bankTransaction.getStrTransactionDate(), formatter));
    bankTransaction.setSalary(bankTransaction.getAmount() + 199.05);
    return bankTransaction;
  }
}
