package com.visiontotale.spring_batch_update_salary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchUpdateSalaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchUpdateSalaryApplication.class, args);
	}

}
