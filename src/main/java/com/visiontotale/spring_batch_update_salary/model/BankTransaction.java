package com.visiontotale.spring_batch_update_salary.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class BankTransaction {
  @Id private Integer transactionId;
  private String beneficiaryName;
  private LocalDate transactionDate;
  @Transient private String strTransactionDate;
  private String accountId;
  private Double amount;
  private Double salary;
}
