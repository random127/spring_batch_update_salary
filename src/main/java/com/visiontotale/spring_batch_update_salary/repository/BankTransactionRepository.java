package com.visiontotale.spring_batch_update_salary.repository;

import com.visiontotale.spring_batch_update_salary.model.BankTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankTransactionRepository extends JpaRepository<BankTransaction, Integer> {}
